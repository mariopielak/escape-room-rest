package pl.mariopielak.escaperoom.rest.model;

/**
 * Created by mariopielak on 25.01.2018.
 */
public class Candidate {

    private String name;

    private Status status;

    private String code;

    private String login;

    public Candidate(String name, Status status, String login, String code) {
        this.name = name;
        this.status = status;
        this.login = login;
        this.code = code;

    }

    public String getName() {
        return name;
    }

    public Status getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    public String getLogin() {
        return login;
    }
}
