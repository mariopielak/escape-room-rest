package pl.mariopielak.escaperoom.rest.model;

/**
 * Created by mariopielak on 25.01.2018.
 */
public enum Status {
    DEAD, ALIVE, LOCKED;
}
