package pl.mariopielak.escaperoom.rest.controllers;

import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.mariopielak.escaperoom.rest.model.Candidate;
import pl.mariopielak.escaperoom.rest.model.Status;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/candidates")
@Api(value = "candidates", description = "Rest API for administrative operations", tags = "Admin API")
public class EscapeRoomController {

    private final List<Candidate> candidates = Lists.newArrayList(
            new Candidate("Darth Vader", Status.DEAD, "vader", "abba"),
            new Candidate("Luke Skywalker", Status.ALIVE, "luke", "abba"),
            new Candidate("Batman", Status.LOCKED, "batman", "abba"),
            new Candidate("Spider-man", Status.DEAD, "spider", "abba"),
            new Candidate("Iron man", Status.DEAD, "iron", "abba"),
            new Candidate("Doctor Strange", Status.ALIVE, "doctor", "abba")
            );

    @RequestMapping(value = "/{login}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Get Next Code Amigo", response = Candidate.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 401, message = "Hey Amigo ! Look at the graph. The shortest path is the key!"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public Candidate findByLogin(@PathVariable String login) {
        Optional<Candidate> optionalCandidate = candidates.stream().filter(e -> login.equals(e.getLogin())).findFirst();
        if (!optionalCandidate.isPresent()) {
            throw new RuntimeException("Candidate not found!");
        }
        return optionalCandidate.get();
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "List of candidates", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 401, message = "Hey Amigo ! Look at the graph. The shortest path is the key!"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public List<String> getAll() {
        return candidates.stream()
                .map(e -> e.getLogin())
                .collect(Collectors.toList());
    }

}